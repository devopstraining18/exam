package com.agiletestingalliance.cpdofsep;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.Duration;

public class DurationTest {
    @Test
    public void test() throws Exception {

        String str = new Duration().dur();
        assertEquals("dur", "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.", str);
        
    }
}

package com.agiletestingalliance.cpdofsep;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.TestGetStr;

public class TestGetStrTest {
    @Test
    public void test() throws Exception {

        String str = new TestGetStr("myString").gstr();
        assertEquals("Text", "myString", str);
        
    }
}

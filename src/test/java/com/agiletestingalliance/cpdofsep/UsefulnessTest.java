package com.agiletestingalliance.cpdofsep;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.Usefulness;

public class UsefulnessTest {
    @Test
    public void testDesc() throws Exception {

        String str = new Usefulness().desc();
        assertEquals("Text", "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.", str);
        
    }
   

    @Test
    public void testDummy() throws Exception {

        new Usefulness().functionWF();
    }

}

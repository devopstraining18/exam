package com.agiletestingalliance.cpdofsep;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.MinMax;

public class MinMaxTest {
    @Test
    public void testMax() throws Exception {

        int res = new MinMax().max(3,6);
        assertEquals("max", 6, res);
        
    }

    @Test
    public void testMax2() throws Exception {

        int res = new MinMax().max(6,3);
        assertEquals("max", 6, res);

    }

    @Test
    public void testStr() throws Exception {

        String res = new MinMax().bar("String");
        assertEquals("String", "String", res);

    }

    @Test
    public void testNull() throws Exception {

        String res = new MinMax().bar(null);
        assertEquals("null", null, res);

    }


}
